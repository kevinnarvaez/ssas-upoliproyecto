Repositorio de Proyecto de Clase: Implementación y Mantenimiento de SQL Server 2008R2 Analysis Services

Resumen del Proyecto
	a. Modelado de OLTP
	b. Construcción de OLTP
	c. Carga de Datos
	d. Modelado de OLAP
	e. Construcción de BD OLAP
	f. Procedimientos de Carga de OLAP
	g. Construcción de Cubos
	h. Explorar Cubos por Medio de Excel
	
Objetivo del Proyecto:
	* Construir un cubo mediante SQL Server 2008R2
	
Descripción:

El proyecto estará dividido en cuatro fases:
	
	Fase I: Inicial y de Preparación
		Entregables:
		a. Entrevistas
		b. Bitacoras de reuniones
		c. Documento de Visión
		d. Cronograma de Trabajo
		e. Modelado Inicial de OLTP
	Fase II: Modelado de OLTP
		a. Modelado Terminado de OLTP
		b. DDL Script
		c. Excel - Construcción de Data
		e. Script Poblado de Datos
		f. Documento de Requisitos de Sistemas
		g. Backup de BD SQL Server
		h. Diccionario de Datos
	Fase III: Modelado de OLAP
		a. Modelado Inicial de OLAP
		b. Diccionario de Datos
		c. Construcción de SP (Store Procedure) para poblar OLAP
		d. Creación de JOB para ejecución de Carga de OLAP
	Fase IV: Construcción de Cubo
		a. Creación de Cubo en Microsoft Visual Studio 2008
			* El cubo debe incluir todos los elementos vistos en clase (Cálculos, Agregaciones, KPI, etc.)
			* El entregable es el proyecto.
		b. Exploración de Cubo por Medio de Excel.
			* Varios Libros de Excel, donde se muestre la consulta de los diferentes escenarios.
		
	
	