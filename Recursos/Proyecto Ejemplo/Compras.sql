/*
 * ER/Studio 8.0 SQL Code Generation
 * Company :      Microsoft
 * Project :      Compras.dm1
 * Author :       Microsoft
 *
 * Date Created : Sunday, May 19, 2013 13:40:00
 * Target DBMS : Microsoft SQL Server 2008
 */

USE master
go
CREATE DATABASE ComprasDB
go
USE ComprasDB
go
/* 
 * TABLE: Categoria 
 */

CREATE TABLE Categoria(
    CategoriaID    int             IDENTITY(1,1),
    Descripcion    nvarchar(50)    NOT NULL,
    CONSTRAINT PK2 PRIMARY KEY CLUSTERED (CategoriaID)
)
go



IF OBJECT_ID('Categoria') IS NOT NULL
    PRINT '<<< CREATED TABLE Categoria >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Categoria >>>'
go

/* 
 * TABLE: Cliente 
 */

CREATE TABLE Cliente(
    ClienteID    int              IDENTITY(1,1),
    Nombre       nvarchar(100)    NOT NULL,
    CONSTRAINT PK6 PRIMARY KEY CLUSTERED (ClienteID)
)
go



IF OBJECT_ID('Cliente') IS NOT NULL
    PRINT '<<< CREATED TABLE Cliente >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Cliente >>>'
go

/* 
 * TABLE: Compra 
 */

CREATE TABLE Compra(
    CompraID     int     NOT NULL,
    Fecha        date    NULL,
    ClienteID    int     NOT NULL,
    CONSTRAINT PK4 PRIMARY KEY CLUSTERED (CompraID)
)
go



IF OBJECT_ID('Compra') IS NOT NULL
    PRINT '<<< CREATED TABLE Compra >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Compra >>>'
go

/* 
 * TABLE: CompraDetalle 
 */

CREATE TABLE CompraDetalle(
    CompraDetalleID    int               IDENTITY(1,1),
    CompraID           int               NOT NULL,
    ProductoID         int               NOT NULL,
    Precio             numeric(10, 2)    NOT NULL,
    Cantidad           numeric(10, 0)    NULL,
    CONSTRAINT PK5 PRIMARY KEY CLUSTERED (CompraDetalleID)
)
go



IF OBJECT_ID('CompraDetalle') IS NOT NULL
    PRINT '<<< CREATED TABLE CompraDetalle >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE CompraDetalle >>>'
go

/* 
 * TABLE: Producto 
 */

CREATE TABLE Producto(
    ProductoID     int               IDENTITY(1,1),
    Nombre         nvarchar(50)      NOT NULL,
    Precio         numeric(18, 2)    NOT NULL,
    CategoriaID    int               NOT NULL,
    CONSTRAINT PK1 PRIMARY KEY CLUSTERED (ProductoID)
)
go



IF OBJECT_ID('Producto') IS NOT NULL
    PRINT '<<< CREATED TABLE Producto >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Producto >>>'
go

/* 
 * INDEX: Ref62 
 */

CREATE INDEX Ref62 ON Compra(ClienteID)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('Compra') AND name='Ref62')
    PRINT '<<< CREATED INDEX Compra.Ref62 >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX Compra.Ref62 >>>'
go

/* 
 * TABLE: Compra 
 */

ALTER TABLE Compra ADD CONSTRAINT RefCliente2 
    FOREIGN KEY (ClienteID)
    REFERENCES Cliente(ClienteID)
go


/* 
 * TABLE: CompraDetalle 
 */

ALTER TABLE CompraDetalle ADD CONSTRAINT RefCompra4 
    FOREIGN KEY (CompraID)
    REFERENCES Compra(CompraID)
go

ALTER TABLE CompraDetalle ADD CONSTRAINT RefProducto5 
    FOREIGN KEY (ProductoID)
    REFERENCES Producto(ProductoID)
go


/* 
 * TABLE: Producto 
 */

ALTER TABLE Producto ADD CONSTRAINT RefCategoria1 
    FOREIGN KEY (CategoriaID)
    REFERENCES Categoria(CategoriaID)
go


