/*
 * ER/Studio 8.0 SQL Code Generation
 * Company :      Hewlett-Packard
 * Project :      Inventario.DM1
 * Author :       hp
 *
 * Date Created : Wednesday, June 12, 2013 13:22:40
 * Target DBMS : Microsoft SQL Server 2008
 */

USE master
go
CREATE DATABASE InventarioOLTP
go
USE InventarioOLTP
go
/* 
 * TABLE: AbonoDetalle 
 */

CREATE TABLE AbonoDetalle(
    IdAbono       int               NOT NULL,
    id_venta      int               NOT NULL,
    FechaAbono    date              NOT NULL,
    MontoAbono    numeric(12, 2)    NULL,
    CONSTRAINT PK15 PRIMARY KEY NONCLUSTERED (IdAbono, id_venta)
)
go



IF OBJECT_ID('AbonoDetalle') IS NOT NULL
    PRINT '<<< CREATED TABLE AbonoDetalle >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE AbonoDetalle >>>'
go

/* 
 * TABLE: AbonoVenta 
 */

CREATE TABLE AbonoVenta(
    IdAbono      int                 IDENTITY(1,1),
    no_doc       char(50)            NOT NULL,
    FechaDoc     date                NOT NULL,
    Monto        double precision    NOT NULL,
    Anulada      char(10)            NOT NULL,
    IdCliente    int                 NOT NULL,
    CONSTRAINT PK2 PRIMARY KEY NONCLUSTERED (IdAbono)
)
go



IF OBJECT_ID('AbonoVenta') IS NOT NULL
    PRINT '<<< CREATED TABLE AbonoVenta >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE AbonoVenta >>>'
go

/* 
 * TABLE: Categoria 
 */

CREATE TABLE Categoria(
    IdCategoria        int              IDENTITY(1,1),
    NombreCategoria    nvarchar(100)    NOT NULL,
    CONSTRAINT PK3 PRIMARY KEY NONCLUSTERED (IdCategoria)
)
go



IF OBJECT_ID('Categoria') IS NOT NULL
    PRINT '<<< CREATED TABLE Categoria >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Categoria >>>'
go

/* 
 * TABLE: Cliente 
 */

CREATE TABLE Cliente(
    IdCliente    int                 IDENTITY(1,1),
    Cedula       char(16)            NOT NULL,
    Nombre       nvarchar(100)       NOT NULL,
    Ciudad       nvarchar(10)        NOT NULL,
    Correo       nvarchar(10)        NOT NULL,
    Direccion    nvarchar(100)       NOT NULL,
    Telefono     int                 NOT NULL,
    DiasCred     tinyint             NOT NULL,
    MaxCred      double precision    NOT NULL,
    CONSTRAINT PK6 PRIMARY KEY NONCLUSTERED (IdCliente)
)
go



IF OBJECT_ID('Cliente') IS NOT NULL
    PRINT '<<< CREATED TABLE Cliente >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Cliente >>>'
go

/* 
 * TABLE: DetalleSalida 
 */

CREATE TABLE DetalleSalida(
    IdProducto    int                 NOT NULL,
    IdSalida      int                 NOT NULL,
    Cantidad      double precision    NOT NULL,
    Precio        double precision    NOT NULL,
    CONSTRAINT PK9 PRIMARY KEY NONCLUSTERED (IdProducto, IdSalida)
)
go



IF OBJECT_ID('DetalleSalida') IS NOT NULL
    PRINT '<<< CREATED TABLE DetalleSalida >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE DetalleSalida >>>'
go

/* 
 * TABLE: Entrada 
 */

CREATE TABLE Entrada(
    IdEntrada    int              IDENTITY(1,1),
    Nombre       nvarchar(100)    NOT NULL,
    CONSTRAINT PK7 PRIMARY KEY NONCLUSTERED (IdEntrada)
)
go



IF OBJECT_ID('Entrada') IS NOT NULL
    PRINT '<<< CREATED TABLE Entrada >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Entrada >>>'
go

/* 
 * TABLE: EntradaDetalle 
 */

CREATE TABLE EntradaDetalle(
    IdEntrada     int                 NOT NULL,
    IdProducto    int                 NOT NULL,
    Fecha         date                NOT NULL,
    Cantidad      double precision    NOT NULL,
    Precio        double precision    NOT NULL,
    CONSTRAINT PK10 PRIMARY KEY NONCLUSTERED (IdEntrada, IdProducto)
)
go



IF OBJECT_ID('EntradaDetalle') IS NOT NULL
    PRINT '<<< CREATED TABLE EntradaDetalle >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE EntradaDetalle >>>'
go

/* 
 * TABLE: Producto 
 */

CREATE TABLE Producto(
    IdProducto             int                 IDENTITY(1,1),
    DescripcionProducto    nvarchar(100)       NOT NULL,
    Costo                  double precision    NOT NULL,
    StockMax               double precision    NOT NULL,
    StockMin               double precision    NOT NULL,
    Existencia             double precision    NOT NULL,
    IdCategoria            int                 NOT NULL,
    CONSTRAINT PK4 PRIMARY KEY NONCLUSTERED (IdProducto)
)
go



IF OBJECT_ID('Producto') IS NOT NULL
    PRINT '<<< CREATED TABLE Producto >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Producto >>>'
go

/* 
 * TABLE: Salida 
 */

CREATE TABLE Salida(
    IdSalida       int              IDENTITY(1,1),
    FechaDoc       date             NOT NULL,
    NoDoc          char(10)         NOT NULL,
    Descripcion    nvarchar(100)    NOT NULL,
    CONSTRAINT PK8 PRIMARY KEY NONCLUSTERED (IdSalida)
)
go



IF OBJECT_ID('Salida') IS NOT NULL
    PRINT '<<< CREATED TABLE Salida >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Salida >>>'
go

/* 
 * TABLE: Venta 
 */

CREATE TABLE Venta(
    id_venta         int                 IDENTITY(1,1),
    Vendedor         nvarchar(100)       NOT NULL,
    NoFactura        char(20)            NOT NULL,
    FechaDoc         date                NOT NULL,
    fecha_vence      date                NOT NULL,
    Cancelada        int                 NOT NULL,
    Descuento        double precision    NOT NULL,
    Saldo            double precision    NOT NULL,
    Observaciones    nvarchar(10)        NOT NULL,
    IdCliente        int                 NOT NULL,
    CONSTRAINT PK11 PRIMARY KEY NONCLUSTERED (id_venta)
)
go



IF OBJECT_ID('Venta') IS NOT NULL
    PRINT '<<< CREATED TABLE Venta >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Venta >>>'
go

/* 
 * TABLE: VentaDetalle 
 */

CREATE TABLE VentaDetalle(
    IdProducto    int                 NOT NULL,
    id_venta      int                 NOT NULL,
    cantidad      double precision    NOT NULL,
    Precio        double precision    NOT NULL,
    descuento     double precision    NOT NULL,
    CONSTRAINT PK12 PRIMARY KEY NONCLUSTERED (IdProducto, id_venta)
)
go



IF OBJECT_ID('VentaDetalle') IS NOT NULL
    PRINT '<<< CREATED TABLE VentaDetalle >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE VentaDetalle >>>'
go

/* 
 * TABLE: AbonoDetalle 
 */

ALTER TABLE AbonoDetalle ADD CONSTRAINT RefAbonoVenta18 
    FOREIGN KEY (IdAbono)
    REFERENCES AbonoVenta(IdAbono)
go

ALTER TABLE AbonoDetalle ADD CONSTRAINT RefVenta19 
    FOREIGN KEY (id_venta)
    REFERENCES Venta(id_venta)
go


/* 
 * TABLE: AbonoVenta 
 */

ALTER TABLE AbonoVenta ADD CONSTRAINT RefCliente20 
    FOREIGN KEY (IdCliente)
    REFERENCES Cliente(IdCliente)
go


/* 
 * TABLE: DetalleSalida 
 */

ALTER TABLE DetalleSalida ADD CONSTRAINT RefSalida4 
    FOREIGN KEY (IdSalida)
    REFERENCES Salida(IdSalida)
go

ALTER TABLE DetalleSalida ADD CONSTRAINT RefProducto5 
    FOREIGN KEY (IdProducto)
    REFERENCES Producto(IdProducto)
go


/* 
 * TABLE: EntradaDetalle 
 */

ALTER TABLE EntradaDetalle ADD CONSTRAINT RefProducto6 
    FOREIGN KEY (IdProducto)
    REFERENCES Producto(IdProducto)
go

ALTER TABLE EntradaDetalle ADD CONSTRAINT RefEntrada7 
    FOREIGN KEY (IdEntrada)
    REFERENCES Entrada(IdEntrada)
go


/* 
 * TABLE: Producto 
 */

ALTER TABLE Producto ADD CONSTRAINT RefCategoria1 
    FOREIGN KEY (IdCategoria)
    REFERENCES Categoria(IdCategoria)
go


/* 
 * TABLE: Venta 
 */

ALTER TABLE Venta ADD CONSTRAINT RefCliente8 
    FOREIGN KEY (IdCliente)
    REFERENCES Cliente(IdCliente)
go


/* 
 * TABLE: VentaDetalle 
 */

ALTER TABLE VentaDetalle ADD CONSTRAINT RefVenta9 
    FOREIGN KEY (id_venta)
    REFERENCES Venta(id_venta)
go

ALTER TABLE VentaDetalle ADD CONSTRAINT RefProducto10 
    FOREIGN KEY (IdProducto)
    REFERENCES Producto(IdProducto)
go


