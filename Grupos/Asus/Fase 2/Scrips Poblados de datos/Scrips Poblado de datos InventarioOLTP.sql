


SCRIPS PARA POBLAR DATOS InventarioOLTP

use InventarioOLTP;

Tabla Categoria


INSERT INTO [InventarioOLTP].[dbo].[Categoria]([NombreCategoria]) VALUES('Martillo');
INSERT INTO [InventarioOLTP].[dbo].[Categoria]([NombreCategoria]) VALUES('Zinc');
INSERT INTO [InventarioOLTP].[dbo].[Categoria]([NombreCategoria]) VALUES('Ceramica');
INSERT INTO [InventarioOLTP].[dbo].[Categoria]([NombreCategoria]) VALUES('Hierro');
INSERT INTO [InventarioOLTP].[dbo].[Categoria]([NombreCategoria]) VALUES('Aluminio');
INSERT INTO [InventarioOLTP].[dbo].[Categoria]([NombreCategoria]) VALUES('Cemento');
INSERT INTO [Inventario].[dbo].[Categoria]([NombreCategoria]) VALUES('Materiales electricos');
INSERT INTO [InventarioOLTP].[dbo].[Categoria]([NombreCategoria]) VALUES('Tubos PBC');
INSERT INTO [InventarioOLTP].[dbo].[Categoria]([NombreCategoria]) VALUES('Tornillos');
INSERT INTO [InventarioOLTP].[dbo].[Categoria]([NombreCategoria]) VALUES('Ladrillos');

Select * from Categoria 

---------------------------------------------------------------------------

Tabla Cliente

INSERT INTO [InventarioOLTP].[dbo].[Cliente]([Cedula] ,[Nombre] ,[Ciudad],[Correo],[Direccion] ,[Telefono],[DiasCred],[MaxCred])VALUES ('5661004890000H' ,'Kevin', 'Rivas' , 'narvaezk18@yahoo.es' , 'De radio Rumbos tres ciento metros al lago', 83454358, 10, 1000);
INSERT INTO [InventarioOLTP].[dbo].[Cliente]([Cedula] ,[Nombre] ,[Ciudad],[Correo],[Direccion] ,[Telefono],[DiasCred],[MaxCred])VALUES ('5661004890000M' ,'Juan', 'Rivas' , 'sjsaja@ydsd' , 'La milagrosa', 86575656,5,1500);
INSERT INTO [InventarioOLTP].[dbo].[Cliente]([Cedula] ,[Nombre] ,[Ciudad],[Correo],[Direccion] ,[Telefono],[DiasCred],[MaxCred])VALUES ('5661004890000Z' ,'Pedro', 'Leon' , 'dsdh@' , 'Union Fenosa', 85456673,4,500);
INSERT INTO [InventarioOLTP].[dbo].[Cliente]([Cedula] ,[Nombre] ,[Ciudad],[Correo],[Direccion] ,[Telefono],[DiasCred],[MaxCred])VALUES ('5661004890000Z' ,'Gladis', 'Masaya' , 'djddjdjd@kdk' , 'De ENACAL  300 mtrs este', 85675756,8,1400);
INSERT INTO [InventarioOLTP].[dbo].[Cliente]([Cedula] ,[Nombre] ,[Ciudad],[Correo],[Direccion] ,[Telefono],[DiasCred],[MaxCred])VALUES ('5661004890000Z' ,'Pepe', 'Granada' , 'jdjsksk@kdsk' , 'Barrio las brisas', 85675759,4,1200);


select * from dbo.Cliente

------------------------------------------------------------------------------


Tabla Entrada

INSERT INTO [InventarioOLTP].[dbo].[Entrada]([Nombre])VALUES('Cemento Canal');
INSERT INTO [InventarioOLTP].[dbo].[Entrada]([Nombre])VALUES('Tenaza');
INSERT INTO [InventarioOLTP].[dbo].[Entrada]([Nombre])VALUES('Martillo');
INSERT INTO [InventarioOLTP].[dbo].[Entrada]([Nombre])VALUES('Ladrillos');
INSERT INTO [InventarioOLTP].[dbo].[Entrada]([Nombre])VALUES('Aluminio');


Select * from Entrada

------------------------------------------------------------------------------




Tabla Producto


INSERT INTO [InventarioOLTP].[dbo].[Producto]([DescripcionProducto],[Costo] ,[StockMax] ,[StockMin] ,[Existencia] ,[IdCategoria]) VALUES('Cemento Canal', 300,100,10,50,6);
INSERT INTO [InventarioOLTP].[dbo].[Producto]([DescripcionProducto],[Costo] ,[StockMax] ,[StockMin] ,[Existencia] ,[IdCategoria]) VALUES('Cemento Holcim', 280,100,10,66,6);
INSERT INTO [InventarioOLTP].[dbo].[Producto]([DescripcionProducto],[Costo] ,[StockMax] ,[StockMin] ,[Existencia] ,[IdCategoria]) VALUES('Tornillos de cinco pulgada', 10,500,50,300,9);
INSERT INTO [InventarioOLTP].[dbo].[Producto]([DescripcionProducto],[Costo] ,[StockMax] ,[StockMin] ,[Existencia] ,[IdCategoria]) VALUES('Tornillos de 3 pulgadas', 7,600,100,250,9);
INSERT INTO [InventarioOLTP].[dbo].[Producto]([DescripcionProducto],[Costo] ,[StockMax] ,[StockMin] ,[Existencia] ,[IdCategoria]) VALUES('Conductor Duple', 10,100,10,46,7);
INSERT INTO [InventarioOLTP].[dbo].[Producto]([DescripcionProducto],[Costo] ,[StockMax] ,[StockMin] ,[Existencia] ,[IdCategoria]) VALUES('Tenaza', 46,30,5,20,7);
INSERT INTO [InventarioOLTP].[dbo].[Producto]([DescripcionProducto],[Costo] ,[StockMax] ,[StockMin] ,[Existencia] ,[IdCategoria]) VALUES('Varilla de hierro larga', 300,100,5,45,4);
INSERT INTO [InventarioOLTP].[dbo].[Producto]([DescripcionProducto],[Costo] ,[StockMax] ,[StockMin] ,[Existencia] ,[IdCategoria]) VALUES('Varilla de hierro corta', 220,100,5,35,4);
INSERT INTO [InventarioOLTP].[dbo].[Producto]([DescripcionProducto],[Costo] ,[StockMax] ,[StockMin] ,[Existencia] ,[IdCategoria]) VALUES('Tubos PBC', 10,50,10,44,8);
INSERT INTO [InventarioOLTP].[dbo].[Producto]([DescripcionProducto],[Costo] ,[StockMax] ,[StockMin] ,[Existencia] ,[IdCategoria]) VALUES('Zinc de cinco pies', 200,100,5,48,2);
INSERT INTO [InventarioOLTP].[dbo].[Producto]([DescripcionProducto],[Costo] ,[StockMax] ,[StockMin] ,[Existencia] ,[IdCategoria]) VALUES('Zinc de dies pies', 280,100,5,34,2);
INSERT INTO [InventarioOLTP].[dbo].[Producto]([DescripcionProducto],[Costo] ,[StockMax] ,[StockMin] ,[Existencia] ,[IdCategoria]) VALUES('Martillo', 50,50,5,25,1);
INSERT INTO [InventarioOLTP].[dbo].[Producto]([DescripcionProducto],[Costo] ,[StockMax] ,[StockMin] ,[Existencia] ,[IdCategoria]) VALUES('Ceramica', 40,50,5,34,3);
INSERT INTO [InventarioOLTP].[dbo].[Producto]([DescripcionProducto],[Costo] ,[StockMax] ,[StockMin] ,[Existencia] ,[IdCategoria]) VALUES('Aluminio', 30,30,5,20,5);
INSERT INTO [InventarioOLTP].[dbo].[Producto]([DescripcionProducto],[Costo] ,[StockMax] ,[StockMin] ,[Existencia] ,[IdCategoria]) VALUES('Ladrillos', 2,1000,50,234,10);



select * from Producto;

----------------------------------------------------------------------------------



Tabla AbonoVenta


delete from AbonoVenta
DBCC CheckIndex('AbonoVenta', reseed, 0);
DBCC CheckIndex('AbonoVenta', reseed);

INSERT INTO [InventarioOLTP].[dbo].[AbonoVenta]([no_doc],[FechaDoc],[Monto],[Anulada],[IdCliente]) VALUES(1212,'2013/5/22',1000,'No',3);
INSERT INTO [InventarioOLTP].[dbo].[AbonoVenta]([no_doc],[FechaDoc],[Monto],[Anulada],[IdCliente]) VALUES(1315,'2013/5/30',500,'No',4);

select * from AbonoVenta


---------------------------------------------------------------------------------



Tabla EntradaDetalle


INSERT INTO [InventarioOLTP].[dbo].[EntradaDetalle]([IdEntrada],[IdProducto],[Fecha],[Cantidad],[Precio])VALUES(1,1,'2013/5/7',50,300);
INSERT INTO [InventarioOLTP].[dbo].[EntradaDetalle]([IdEntrada],[IdProducto],[Fecha],[Cantidad],[Precio])VALUES(2,6,'2013/5/7',40,10);
INSERT INTO [InventarioOLTP].[dbo].[EntradaDetalle]([IdEntrada],[IdProducto],[Fecha],[Cantidad],[Precio])VALUES(3,12,'2013/5/7',45,50);
INSERT INTO [InventarioOLTP].[dbo].[EntradaDetalle]([IdEntrada],[IdProducto],[Fecha],[Cantidad],[Precio])VALUES(4,15,'2013/5/7',100,4);
INSERT INTO [InventarioOLTP].[dbo].[EntradaDetalle]([IdEntrada],[IdProducto],[Fecha],[Cantidad],[Precio])VALUES(5,14,'2013/5/7',30,30);

Select EntradaDetalle.IdEntrada,Producto.DescripcionProducto,EntradaDetalle.Fecha,EntradaDetalle.Cantidad,EntradaDetalle.Precio from EntradaDetalle,Producto where Producto.IdProducto=EntradaDetalle.IdProducto;


--------------------------------------------------------------------------------


Tabla Salida

INSERT INTO [InventarioOLTP].[dbo].[Salida]([FechaDoc],[NoDoc],[Descripcion])VALUES('2013/5/22',1212,'Salidas conformes');
INSERT INTO [InventarioOLTP].[dbo].[Salida]([FechaDoc],[NoDoc],[Descripcion])VALUES('2013/5/30',1315,'Salidas conformes');
INSERT INTO [InventarioOLTP].[dbo].[Salida]([FechaDoc],[NoDoc],[Descripcion])VALUES('2013/6/1',4455,'Salidas conformes');
INSERT INTO [InventarioOLTP].[dbo].[Salida]([FechaDoc],[NoDoc],[Descripcion])VALUES('2013/6/1',2398,'Salidas conformes');
INSERT INTO [InventarioOLTP].[dbo].[Salida]([FechaDoc],[NoDoc],[Descripcion])VALUES('2013/6/1',3610,'Salidas conformes');

select * from Salida 

-------------------------------------------------------------------------------

Tabla DetalleSalida

INSERT INTO [InventarioOLTP].[dbo].[DetalleSalida]([IdProducto],[IdSalida],[Cantidad],[Precio])VALUES(1,1,20,320);
INSERT INTO [InventarioOLTP].[dbo].[DetalleSalida]([IdProducto],[IdSalida],[Cantidad],[Precio])VALUES(6,2,10,20);
INSERT INTO [InventarioOLTP].[dbo].[DetalleSalida]([IdProducto],[IdSalida],[Cantidad],[Precio])VALUES(12,3,15,66);
INSERT INTO [InventarioOLTP].[dbo].[DetalleSalida]([IdProducto],[IdSalida],[Cantidad],[Precio])VALUES(15,4,55,8);
INSERT INTO [InventarioOLTP].[dbo].[DetalleSalida]([IdProducto],[IdSalida],[Cantidad],[Precio])VALUES(14,5,18,45);

Select DetalleSalida.IdSalida,Producto.DescripcionProducto,DetalleSalida.Cantidad,DetalleSalida.Precio,Total=DetalleSalida.Cantidad *DetalleSalida .Precio   from DetalleSalida,Producto where Producto.IdProducto=DetalleSalida .IdProducto  

------------------------------------------------------------------------------

Tabla Venta


INSERT INTO [InventarioOLTP].[dbo].[Venta]([Vendedor],[NoFactura],[FechaDoc],[fecha_vence],[Cancelada],[Descuento],[Saldo],[Observaciones],[IdCliente])VALUES('Juan Francisco Gaytan',2752,'2013/5/22','2013/5/30','No', 0,500,'Solo estan abonando',3);

INSERT INTO [InventarioOLTP].[dbo].[Venta]([Vendedor],[NoFactura],[FechaDoc],[fecha_vence],[Cancelada],[Descuento],[Saldo],[Observaciones],[IdCliente])VALUES('Juan Francisco Gaytan',2728,'2013/5/30','2013/6/2','No', 0,1000,'Solo estan abonando',4);
INSERT INTO [InventarioOLTP].[dbo].[Venta]([Vendedor],[NoFactura],[FechaDoc],[fecha_vence],[Cancelada],[Descuento],[Saldo],[Observaciones],[IdCliente])VALUES('Juan Francisco Gaytan',2882,'2013/6/1','2013/5/6','SI', 0,0,'Estos No deben nada',2);

INSERT INTO [InventarioOLTP].[dbo].[Venta]([Vendedor],[NoFactura],[FechaDoc],[fecha_vence],[Cancelada],[Descuento],[Saldo],[Observaciones],[IdCliente])VALUES('Juan Francisco Gaytan',2785,'2013/6/1','2013/6/5','SI', 0,0,'Estos No deben nada',6);
INSERT INTO [InventarioOLTP].[dbo].[Venta]([Vendedor],[NoFactura],[FechaDoc],[fecha_vence],[Cancelada],[Descuento],[Saldo],[Observaciones],[IdCliente])VALUES('Juan Francisco Gaytan',2983,'2013/6/1','2013/5/6','SI', 0,0,'Estos No deben nada',7);



select Venta.id_venta,Cliente.Nombre, Venta.Vendedor,Venta.NoFactura,Venta.FechaDoc,Venta.fecha_vence,Venta.Cancelada,Venta .Descuento,Venta.Saldo,Venta.Observaciones from Venta,Cliente where Venta.IdCliente= Cliente.IdCliente  


-----------------------------------------------------------------------------------

Tabla VentaDetalle


INSERT INTO [InventarioOLTP].[dbo].[VentaDetalle]([IdProducto],[id_venta] ,[cantidad] ,[Precio] ,[descuento])VALUES(1,1,20,320,0);
INSERT INTO [InventarioOLTP].[dbo].[VentaDetalle]([IdProducto],[id_venta] ,[cantidad] ,[Precio] ,[descuento])VALUES(6,2,10,20,0);
INSERT INTO [InventarioOLTP].[dbo].[VentaDetalle]([IdProducto],[id_venta] ,[cantidad] ,[Precio] ,[descuento])VALUES(12,8,15,66,0);
INSERT INTO [InventarioOLTP].[dbo].[VentaDetalle]([IdProducto],[id_venta] ,[cantidad] ,[Precio] ,[descuento])VALUES(15,7,55,8,0);
INSERT INTO [InventarioOLTP].[dbo].[VentaDetalle]([IdProducto],[id_venta] ,[cantidad] ,[Precio] ,[descuento])VALUES(14,9,18,45,0);


Select VentaDetalle.id_venta,Venta.IdCliente  ,Producto.DescripcionProducto,VentaDetalle.cantidad,VentaDetalle.Precio,VentaDetalle.descuento,Total= VentaDetalle .cantidad *VentaDetalle .Precio   from VentaDetalle,Producto,Venta   where VentaDetalle .IdProducto =Producto .IdProducto and VentaDetalle.id_venta =Venta.id_venta  


Select Total=SUM (VentaDetalle .cantidad *VentaDetalle .Precio) from VentaDetalle 

------------------------------------------------------------------------------------


Tabla AbonoDetalle


use InventarioOLTP 

INSERT INTO [InventarioOLTP].[dbo].[AbonoDetalle]([IdAbono],[id_venta],[FechaAbono],[MontoAbono]) VALUES(5,1,'2013/5/22',1000);
INSERT INTO [InventarioOLTP].[dbo].[AbonoDetalle]([IdAbono],[id_venta],[FechaAbono],[MontoAbono]) VALUES(6,2,'2013/5/30',500);

select * from AbonoDetalle 

Select Total=Sum(AbonoDetalle .MontoAbono) from AbonoDetalle 
 








