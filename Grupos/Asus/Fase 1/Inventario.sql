/*
 * ER/Studio 8.0 SQL Code Generation
 * Company :      Hewlett-Packard
 * Project :      Avances.DM1
 * Author :       hp
 *
 * Date Created : Tuesday, May 28, 2013 22:55:38
 * Target DBMS : Microsoft SQL Server 2008
 */

USE master
go
CREATE DATABASE Inventario
go
USE Inventario
go
/* 
 * TABLE: abono_venta 
 */

CREATE TABLE abono_venta(
    id_abono      int                 IDENTITY(1,1),
    no_doc        char(50)            NOT NULL,
    fecha_doc     date                NOT NULL,
    monto         double precision    NOT NULL,
    anulada       char(10)            NOT NULL,
    id_venta      int                 NOT NULL,
    id_cliente    int                 NOT NULL,
    CONSTRAINT PK2 PRIMARY KEY NONCLUSTERED (id_abono)
)
go



IF OBJECT_ID('abono_venta') IS NOT NULL
    PRINT '<<< CREATED TABLE abono_venta >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE abono_venta >>>'
go

/* 
 * TABLE: AbonoCompra 
 */

CREATE TABLE AbonoCompra(
    id_abono        int                 IDENTITY(1,1),
    no_doc          int                 NOT NULL,
    fecha_doc       date                NOT NULL,
    monto           double precision    NOT NULL,
    anulada         char(10)            NOT NULL,
    id_proveedor    int                 NOT NULL,
    id_entrada      int                 NOT NULL,
    CONSTRAINT PK1 PRIMARY KEY NONCLUSTERED (id_abono)
)
go



IF OBJECT_ID('AbonoCompra') IS NOT NULL
    PRINT '<<< CREATED TABLE AbonoCompra >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE AbonoCompra >>>'
go

/* 
 * TABLE: categoria 
 */

CREATE TABLE categoria(
    id_categoria        int              IDENTITY(1,1),
    nombre_categoria    nvarchar(100)    NOT NULL,
    CONSTRAINT PK3 PRIMARY KEY NONCLUSTERED (id_categoria)
)
go



IF OBJECT_ID('categoria') IS NOT NULL
    PRINT '<<< CREATED TABLE categoria >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE categoria >>>'
go

/* 
 * TABLE: clientes 
 */

CREATE TABLE clientes(
    id_cliente        int                 IDENTITY(1,1),
    cedula            char(16)            NOT NULL,
    nombre_cliente    nvarchar(100)       NOT NULL,
    ciudad            nvarchar(10)        NOT NULL,
    correo            nvarchar(10)        NOT NULL,
    direccion         nvarchar(100)       NOT NULL,
    telefono          int                 NOT NULL,
    dias_cred         tinyint             NOT NULL,
    max_cred          double precision    NOT NULL,
    saldo             double precision    NOT NULL,
    CONSTRAINT PK6 PRIMARY KEY NONCLUSTERED (id_cliente)
)
go



IF OBJECT_ID('clientes') IS NOT NULL
    PRINT '<<< CREATED TABLE clientes >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE clientes >>>'
go

/* 
 * TABLE: cuentasxcobrar 
 */

CREATE TABLE cuentasxcobrar(
    no_doc        char(100)           NOT NULL,
    fecha_doc     date                NOT NULL,
    fecha_venc    date                NOT NULL,
    total         double precision    NOT NULL,
    abono         double precision    NOT NULL,
    cancelada     char(18)            NOT NULL,
    credito       char(18)            NOT NULL,
    anulada       char(10)            NOT NULL,
    id_venta      int                 NOT NULL,
    id_cliente    int                 NOT NULL
)
go



IF OBJECT_ID('cuentasxcobrar') IS NOT NULL
    PRINT '<<< CREATED TABLE cuentasxcobrar >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE cuentasxcobrar >>>'
go

/* 
 * TABLE: cuentasxpagar 
 */

CREATE TABLE cuentasxpagar(
    no_doc          char(100)           NOT NULL,
    fecha_doc       date                NOT NULL,
    fecha_venc      date                NOT NULL,
    total           double precision    NOT NULL,
    abono           float               NOT NULL,
    cancelada       char(10)            NOT NULL,
    credito         double precision    NOT NULL,
    anulada         char(10)            NOT NULL,
    id_entrada      int                 NOT NULL,
    id_proveedor    int                 NOT NULL
)
go



IF OBJECT_ID('cuentasxpagar') IS NOT NULL
    PRINT '<<< CREATED TABLE cuentasxpagar >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE cuentasxpagar >>>'
go

/* 
 * TABLE: detalle_salida 
 */

CREATE TABLE detalle_salida(
    cantidad       double precision    NOT NULL,
    precio         double precision    NOT NULL,
    total          double precision    NOT NULL,
    id_salida      int                 NOT NULL,
    id_producto    int                 NOT NULL
)
go



IF OBJECT_ID('detalle_salida') IS NOT NULL
    PRINT '<<< CREATED TABLE detalle_salida >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE detalle_salida >>>'
go

/* 
 * TABLE: entrada 
 */

CREATE TABLE entrada(
    id_entrada      int                 IDENTITY(1,1),
    fecha_doc       date                NOT NULL,
    no_doc          char(100)           NOT NULL,
    credito         char(100)           NOT NULL,
    fecha_venc      date                NOT NULL,
    concepto        nvarchar(100)       NOT NULL,
    subtotal        double precision    NOT NULL,
    descuento       double precision    NOT NULL,
    gastos          double precision    NOT NULL,
    iva             double precision    NOT NULL,
    total           double precision    NOT NULL,
    cancelada       char(18)            NOT NULL,
    anulada         char(10)            NOT NULL,
    id_proveedor    int                 NOT NULL,
    CONSTRAINT PK7 PRIMARY KEY NONCLUSTERED (id_entrada)
)
go



IF OBJECT_ID('entrada') IS NOT NULL
    PRINT '<<< CREATED TABLE entrada >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE entrada >>>'
go

/* 
 * TABLE: entrada_detalle 
 */

CREATE TABLE entrada_detalle(
    cantidad       double precision    NOT NULL,
    bono           double precision    NOT NULL,
    precio         double precision    NOT NULL,
    descuento      double precision    NOT NULL,
    iva            char(100)           NOT NULL,
    total          double precision    NOT NULL,
    id_producto    int                 NOT NULL,
    id_entrada     int                 NOT NULL
)
go



IF OBJECT_ID('entrada_detalle') IS NOT NULL
    PRINT '<<< CREATED TABLE entrada_detalle >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE entrada_detalle >>>'
go

/* 
 * TABLE: producto 
 */

CREATE TABLE producto(
    id_producto             int                 IDENTITY(1,1),
    descripcion_producto    nvarchar(100)       NOT NULL,
    precio_uni              double precision    NOT NULL,
    costo_promedio          double precision    NOT NULL,
    costo_ultimo            double precision    NOT NULL,
    stock_max               double precision    NOT NULL,
    stock_min               double precision    NOT NULL,
    existencia              double precision    NOT NULL,
    estado                  nvarchar(10)        NOT NULL,
    id_categoria            int                 NOT NULL,
    CONSTRAINT PK4 PRIMARY KEY NONCLUSTERED (id_producto)
)
go



IF OBJECT_ID('producto') IS NOT NULL
    PRINT '<<< CREATED TABLE producto >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE producto >>>'
go

/* 
 * TABLE: proveedores 
 */

CREATE TABLE proveedores(
    id_proveedor        int                 IDENTITY(1,1),
    no_ruc              int                 NOT NULL,
    nombre_proveedor    nvarchar(100)       NOT NULL,
    contacto            nvarchar(100)       NOT NULL,
    ciudad              nvarchar(100)       NOT NULL,
    correo              nvarchar(100)       NOT NULL,
    direccion           nvarchar(100)       NOT NULL,
    telefono            int                 NOT NULL,
    saldo               double precision    NOT NULL,
    CONSTRAINT PK5 PRIMARY KEY NONCLUSTERED (id_proveedor)
)
go



IF OBJECT_ID('proveedores') IS NOT NULL
    PRINT '<<< CREATED TABLE proveedores >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE proveedores >>>'
go

/* 
 * TABLE: salidas 
 */

CREATE TABLE salidas(
    id_salida      int                 IDENTITY(1,1),
    fecha_doc      date                NOT NULL,
    no_doc         char(10)            NOT NULL,
    concepto       nvarchar(100)       NOT NULL,
    descripcion    nvarchar(100)       NOT NULL,
    total          double precision    NOT NULL,
    CONSTRAINT PK8 PRIMARY KEY NONCLUSTERED (id_salida)
)
go



IF OBJECT_ID('salidas') IS NOT NULL
    PRINT '<<< CREATED TABLE salidas >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE salidas >>>'
go

/* 
 * TABLE: venta 
 */

CREATE TABLE venta(
    id_venta          int                 IDENTITY(1,1),
    vendedor          nvarchar(100)       NOT NULL,
    no_factura        char(20)            NOT NULL,
    fecha_doc         date                NOT NULL,
    plan_pago         char(10)            NOT NULL,
    cancelada         int                 NOT NULL,
    fecha_vence       date                NOT NULL,
    nombre_contado    nvarchar(100)       NOT NULL,
    subtotal          double precision    NOT NULL,
    iva               double precision    NOT NULL,
    descuento         double precision    NOT NULL,
    total             double precision    NOT NULL,
    observaciones     nvarchar(10)        NOT NULL,
    anulada           char(10)            NOT NULL,
    id_cliente        int                 NOT NULL,
    CONSTRAINT PK11 PRIMARY KEY NONCLUSTERED (id_venta)
)
go



IF OBJECT_ID('venta') IS NOT NULL
    PRINT '<<< CREATED TABLE venta >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE venta >>>'
go

/* 
 * TABLE: venta_detalle 
 */

CREATE TABLE venta_detalle(
    cantidad        double precision    NOT NULL,
    bono            double precision    NOT NULL,
    precio_uni      double precision    NOT NULL,
    descuento       double precision    NOT NULL,
    iva             double precision    NOT NULL,
    total           double precision    NOT NULL,
    precio_costo    double precision    NOT NULL,
    id_venta        int                 NOT NULL,
    id_producto     int                 NOT NULL
)
go



IF OBJECT_ID('venta_detalle') IS NOT NULL
    PRINT '<<< CREATED TABLE venta_detalle >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE venta_detalle >>>'
go

/* 
 * TABLE: abono_venta 
 */

ALTER TABLE abono_venta ADD CONSTRAINT Refventa16 
    FOREIGN KEY (id_venta)
    REFERENCES venta(id_venta)
go

ALTER TABLE abono_venta ADD CONSTRAINT Refclientes17 
    FOREIGN KEY (id_cliente)
    REFERENCES clientes(id_cliente)
go


/* 
 * TABLE: AbonoCompra 
 */

ALTER TABLE AbonoCompra ADD CONSTRAINT Refproveedores2 
    FOREIGN KEY (id_proveedor)
    REFERENCES proveedores(id_proveedor)
go

ALTER TABLE AbonoCompra ADD CONSTRAINT Refentrada15 
    FOREIGN KEY (id_entrada)
    REFERENCES entrada(id_entrada)
go


/* 
 * TABLE: cuentasxcobrar 
 */

ALTER TABLE cuentasxcobrar ADD CONSTRAINT Refventa13 
    FOREIGN KEY (id_venta)
    REFERENCES venta(id_venta)
go

ALTER TABLE cuentasxcobrar ADD CONSTRAINT Refclientes14 
    FOREIGN KEY (id_cliente)
    REFERENCES clientes(id_cliente)
go


/* 
 * TABLE: cuentasxpagar 
 */

ALTER TABLE cuentasxpagar ADD CONSTRAINT Refentrada11 
    FOREIGN KEY (id_entrada)
    REFERENCES entrada(id_entrada)
go

ALTER TABLE cuentasxpagar ADD CONSTRAINT Refproveedores12 
    FOREIGN KEY (id_proveedor)
    REFERENCES proveedores(id_proveedor)
go


/* 
 * TABLE: detalle_salida 
 */

ALTER TABLE detalle_salida ADD CONSTRAINT Refsalidas4 
    FOREIGN KEY (id_salida)
    REFERENCES salidas(id_salida)
go

ALTER TABLE detalle_salida ADD CONSTRAINT Refproducto5 
    FOREIGN KEY (id_producto)
    REFERENCES producto(id_producto)
go


/* 
 * TABLE: entrada 
 */

ALTER TABLE entrada ADD CONSTRAINT Refproveedores3 
    FOREIGN KEY (id_proveedor)
    REFERENCES proveedores(id_proveedor)
go


/* 
 * TABLE: entrada_detalle 
 */

ALTER TABLE entrada_detalle ADD CONSTRAINT Refproducto6 
    FOREIGN KEY (id_producto)
    REFERENCES producto(id_producto)
go

ALTER TABLE entrada_detalle ADD CONSTRAINT Refentrada7 
    FOREIGN KEY (id_entrada)
    REFERENCES entrada(id_entrada)
go


/* 
 * TABLE: producto 
 */

ALTER TABLE producto ADD CONSTRAINT Refcategoria1 
    FOREIGN KEY (id_categoria)
    REFERENCES categoria(id_categoria)
go


/* 
 * TABLE: venta 
 */

ALTER TABLE venta ADD CONSTRAINT Refclientes8 
    FOREIGN KEY (id_cliente)
    REFERENCES clientes(id_cliente)
go


/* 
 * TABLE: venta_detalle 
 */

ALTER TABLE venta_detalle ADD CONSTRAINT Refventa9 
    FOREIGN KEY (id_venta)
    REFERENCES venta(id_venta)
go

ALTER TABLE venta_detalle ADD CONSTRAINT Refproducto10 
    FOREIGN KEY (id_producto)
    REFERENCES producto(id_producto)
go


