USE [InventarioDW]
GO
/****** Object:  StoredProcedure [dbo].[usp_CargaDimProducto]    Script Date: 06/14/2013 13:54:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Kevin Narvaez
-- Create date: 2013.06.13
-- Description:	Carga Dimension Producto
-- =============================================
ALTER PROCEDURE [dbo].[usp_CargaDimProducto]
AS
BEGIN

	Delete From dbo.DimProducto;

	DBCC CHECKIDENT ('DimProducto', reseed, 0);
	DBCC CHECKIDENT ('DimProducto', reseed);
	
	INSERT INTO [InventarioDW].[dbo].[DimProducto]
           ([ProductoAlternateKey]
           ,[DescripcionProducto])
	 Select IdProducto, DescripcionProducto
	 From InventarioOLTP.dbo.Producto order by IdProducto; 
END
