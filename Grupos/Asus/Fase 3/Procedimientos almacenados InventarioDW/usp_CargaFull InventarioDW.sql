USE [InventarioDW]
GO
/****** Object:  StoredProcedure [dbo].[usp_CargaFullDW]    Script Date: 06/14/2013 13:50:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Kevin Narvaez
-- Create date: 2013.06.14
-- Description:	Carga Completa de DW
-- =============================================
ALTER PROCEDURE [dbo].[usp_CargaFullDW]
AS
BEGIN
	

	exec usp_CargaDimCategoria;
	exec usp_CargaDimCliente;
	exec usp_CargaDimProducto;
	Exec usp_CargaDimTiempo;
	Exec dbo.usp_CargaFactVenta;
	
	
END
