USE [InventarioDW]
GO
/****** Object:  StoredProcedure [dbo].[usp_CargaFactVenta]    Script Date: 06/14/2013 13:53:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Kevin Narváez
-- Create date: 2013.06.13
-- Description:	Carga FactVenta
-- =============================================
ALTER PROCEDURE [dbo].[usp_CargaFactVenta]
AS
BEGIN
	
	--Delete From dbo.FactVenta;
	


	INSERT INTO [InventarioDW].[dbo].[FactVenta]
			   ([ProductoKey]
			   ,[ClienteKey]
			   ,[TiempoKey]
			   ,[CategoriaKey]
			   ,[Precio]
			   ,[Descuento]
			   ,[Cantidad])
		Select DimProducto.ProductoKey
			,DimCliente.ClienteKey
			,DimTiempo.TiempoKey
			,DimCategoria.CategoriaKey
			,InventarioOLTP.dbo.VentaDetalle.Precio 
			,InventarioOLTP.dbo.VentaDetalle.descuento 
			,InventarioOLTP.dbo.VentaDetalle.cantidad
			
			 
		From InventarioOLTP.dbo.Venta
			INNER JOIN InventarioOLTP.dbo.VentaDetalle 
			ON InventarioOLTP.dbo.Venta.id_venta = InventarioOLTP.dbo.VentaDetalle.id_venta
			INNER JOIN dbo.DimCliente 
			ON InventarioOLTP.dbo.Venta.IdCliente = InventarioDW.dbo.DimCliente.ClienteAlternateKey
			INNER JOIN InventarioDW.dbo.DimProducto
			ON InventarioOLTP.dbo.VentaDetalle.IdProducto = InventarioDW.dbo.DimProducto.ProductoAlternateKey
			INNER JOIN InventarioDW.dbo.DimTiempo
			ON InventarioOLTP.dbo.Venta.FechaDoc=InventarioDW.dbo.DimTiempo.TiempoAlternateKey
			INNER JOIN InventarioOLTP.dbo.Producto
		    ON InventarioOLTP.dbo.Producto.IdCategoria = InventarioDW.dbo.DimProducto.ProductoAlternateKey
			INNER JOIN InventarioDW.dbo.DimCategoria
			ON InventarioOLTP.dbo.Producto.IdCategoria = InventarioDW.dbo.DimCategoria.CategoriaAlternateKey
		
					                 

END

