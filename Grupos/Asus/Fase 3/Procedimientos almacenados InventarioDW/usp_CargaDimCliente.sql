USE [InventarioDW]
GO
/****** Object:  StoredProcedure [dbo].[usp_CargaDimCliente]    Script Date: 06/14/2013 13:54:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Kevin Narvaez
-- Create date: 12.06.2013
-- Description:	Carga Dimension Cliente
-- =============================================
ALTER PROCEDURE [dbo].[usp_CargaDimCliente]
AS
BEGIN

	Delete From dbo.DimCliente;

	DBCC CHECKIDENT ('DimCliente', reseed, 0);
	DBCC CHECKIDENT ('DimCliente', reseed);
	
	INSERT INTO [InventarioDW].[dbo].[DimCliente]
           ([ClienteAlternateKey]
           ,[Nombre]
           ,[Direccion]
           ,[Correo]
           ,[Telefono])
     

	 Select IdCliente,Nombre,Direccion,Correo,Telefono
	 From InventarioOLTP.dbo.Cliente order by IdCliente; 
END
