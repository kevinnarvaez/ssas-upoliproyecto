USE [InventarioDW]
GO
/****** Object:  StoredProcedure [dbo].[usp_CargaDimCategoria]    Script Date: 06/14/2013 13:55:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		ASUS
-- Create date: 12/06/13
-- Description:	Carga de datos Tabla DimCategoria
-- =============================================
ALTER PROCEDURE  [dbo].[usp_CargaDimCategoria]
	
AS
BEGIN
	
	Delete from InventarioDW .dbo.DimCategoria 
	
	DBCC CheckIDENT('DimCategoria', reseed, 0);
	DBCC CheckIDENT('DimCategoria', reseed);
	
	INSERT INTO [InventarioDW].[dbo].[DimCategoria]
			([CategoriaAlternateKey]
            ,[Descripcion])
     

	Select IdCategoria,NombreCategoria from InventarioOLTP .dbo .Categoria order by IdCategoria 
	
	
	
	
END

