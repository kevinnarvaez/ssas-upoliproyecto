USE [InventarioDW]
GO
/****** Object:  StoredProcedure [dbo].[usp_CargaDimTiempo]    Script Date: 06/14/2013 13:53:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Kevin Narvaez
-- Create date: 2013.06.13
-- Description:	Carga Dimension Producto
-- =============================================
ALTER PROCEDURE [dbo].[usp_CargaDimTiempo]
AS
BEGIN

	Delete From dbo.DimTiempo;

	DBCC CHECKIDENT ('DimTiempo', reseed, 0);
	DBCC CHECKIDENT ('DimTiempo', reseed);
	
	INSERT INTO [InventarioDW].[dbo].[DimTiempo]
           ([TiempoAlternateKey]
           ,[Mes]
           ,[MesNombre]
           ,[Año])
      
	 Select Distinct FechaDoc,DATEPART(MONTH, InventarioOLTP.dbo.Venta.fechaDoc),Datename(MONTH, InventarioOLTP.dbo.Venta.fechaDoc),DATEPART(Year, InventarioOLTP.dbo.Venta.fechaDoc)
	 From InventarioOLTP.dbo.Venta;
END
